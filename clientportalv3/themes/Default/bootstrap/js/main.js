// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
	animation: "slide",
    slideshow: true,
    slideshowSpeed: 10000,
	pauseOnHover: true,
	animationSpeed: 1000, 
    controlNav: true
  });
});
function signinBoxPopUp() {
	var signinBox = document.getElementById("signin-box");
	if(signinBox.style.display == "none") {
		signinBox.style.display = "block";
	}
	else{
		signinBox.style.display = "none";	
	}
}
if(document.getElementById("header-signin-button").onclick){
	signinBoxPopUp();
}