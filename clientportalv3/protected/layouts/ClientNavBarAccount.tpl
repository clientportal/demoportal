<style>
a:link, a:visited{color:#4f81bd;}

</style>

<div class="panel-group" id="accordion">

    <h4><strong>NAVIGATION</strong></h5>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
					Accounts <span class="glyphicon glyphicon-arrow-down"></span></a>
			</h4>
</div>
		<div id="collapseOne" class="panel-collapse collapse <%= $_GET["page"] == 'client.CompleteViewByAccount'|| $_GET["page"] == 'client.CompleteViewByAllocation' || $_GET["page"] == 'client.TransactionHistory' || $_GET["page"] == 'client.CompleteViewByAllocation' || $_GET["page"] == 'client.DepositFunds' || $_GET["page"] == 'client.WithdrawFunds' || $_GET["page"] == 'client.PasswordReset' || $_GET["page"] == 'client.Profile' ? "in" : "collapse" ; %>">
    <div class="panel-body">
        <table class="table">
            <tr>
				<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.CompleteViewByAccount' ? 'class="active"' : "" ; %>>
					&ndash;&nbsp;<a  href="index.php?page=client.CompleteViewByAccount">Account Summary</a>
                </td>
            </tr>
            <tr>
				<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.TransactionHistory' ? 'class="active"' : "" ; %>>
					&ndash;&nbsp;<a   href="index.php?page=client.TransactionHistory">My Statement</a>
                </td>
            </tr>
            <tr>
				<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.CompleteViewByAllocation' ? 'class="active"' : "" ; %>>
					&ndash;&nbsp;<a  href="index.php?page=client.CompleteViewByAllocation">Asset Allocation</a>
                </td>
            </tr>
			<!--<tr >
				<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.DepositFunds' ? 'class="active"' : "" ; %>>
					&ndash;&nbsp;<a  href="index.php?page=client.DepositFunds">Fund My Account</a>
                </td>
            </tr>-->
            <tr>
				<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.WithdrawFunds' ? 'class="active"' : "" ; %>>
					&ndash;&nbsp;<a  href="index.php?page=client.WithdrawFunds">Withdraw Funds</a>
                </td>
            </tr>
            <tr>
				<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.Profile' ? 'class="active"' : "" ; %>>
					&ndash;&nbsp;<a  href="index.php?page=client.Profile">My Profile</a>
                </td>
            </tr>


        </table>
    </div>
</div>
</div>

	
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
					Trading & Portfolios</a> <span class="glyphicon glyphicon-arrow-down"></span>
			</h4>
    </div>
	<div id="collapseTwo" class="panel-collapse <%= $_GET["page"] == 'client.NewEquityOrders' || $_GET["page"] == 'client.NewFixedDeposit' || $_GET["page"] == 'client.ListEquityOrders' ||$_GET["page"] == 'client.ListFundTransaction' ||
	$_GET["page"] == 'client.NewFundTransaction' || $_GET["page"] == 'client.PortfolioHoldings' || $_GET["page"] == 'client.ListFixedDeposit' || $_GET["page"] == 'client.ManageTradeOrder'  ||  $_GET["page"] == 'client.NewEquityOrder'  ? "in" : "collapse" ; %>">
        <div class="panel-body">
            <table class="table">
                <tr>
					<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.ListEquityOrders' ? 'class="active"' : "" ; %>>
						&ndash;&nbsp;<a  href="index.php?page=client.ListEquityOrders">My Orders</a> 
                    </td>
                </tr>
                <tr>
					<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.NewEquityOrder' ? 'class="active"' : "" ; %>>
						&ndash;&nbsp;<a  href="index.php?page=client.NewEquityOrder">New Order</a> 
                    </td>
                </tr>
                <tr>
					<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.PortfolioHoldings' ? 'class="active"' : "" ; %>>
						&ndash;&nbsp;<a  href="index.php?page=client.PortfolioHoldings">Portfolios</a>
                    </td>
                </tr>
				 <tr>
					<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.ContractNotes' ? 'class="active"' : "" ; %>>
						&ndash;&nbsp;<a  href="index.php?page=client.ContractNotes">My Contract Notes</a>
                    </td>
                </tr>

    <!--            <tr>
					<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.ListFixedDeposit' ? 'class="active"' : "" ; %>>
						&ndash;&nbsp;<a  href="index.php?page=client.ListFixedDeposit">Fixed Deposit</a>
                    </td>
                </tr>
                <tr>
					<td <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.ListFundTransaction' ? 'class="active"' : "" ; %>>
						&ndash;&nbsp;<a  href="index.php?page=client.ListFundTransaction">Fund Transaction</a>
					</td>
                </tr>-->

            </table>
        </div>
    </div>
</div>
<!--<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Quotes & Research <span class="glyphicon glyphicon-arrow-down"></span></a>
			</h4>
    </div>
	<div id="collapseThree" class="panel-collapse <%= $_GET["page"] == 'client.SecurityOverview'  ? "in" : "collapse" ; %> ">
        <div class="panel-body">
            <table class="table">
                <tr>
					<td  <%=isset($_GET[ "page"]) && $_GET["page"] == 'guest.SecurityOverview' ? 'class="active"' : "" ; %>>
						&ndash;&nbsp;<a  href="index.php?page=guest.SecurityOverview">Research </a>
                    </td>
                </tr>
                <tr>
                    <td  <%=isset($_GET[ "page"]) && $_GET["page"] == 'client.MarketOverview' ? 'class="active"' : "" ; %>>
						&ndash;&nbsp;<a  href="index.php?page=client.MarketOverview">Live Market Data</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>-->

</div>