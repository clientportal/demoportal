<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<com:THead>
		<meta charset="utf-8">
		<title>Client Portal</title>

		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="author" content="MTrader LLC">
		<meta name="apple-itunes-app" content="app-id=932775262">
		<meta name="google-play-app" content="app-id=com.zanibal.mtrader">
		<meta name="msApplication-ID" content="App" />
		<meta name="msapplication-TileImage" content="https://lh3.ggpht.com/NK6zSlEmpgwlExA9lqdPdV85XLE1uKf_J5iWtSduh2yjPx5ohY8uzIvjcuYkuQuyEX_Y=w300" />


		<link href="themes/Default/design/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="themes/Default/design/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="themes/Default/design/global/css/components.css" rel="stylesheet">
		<link rel="stylesheet" href="themes/Default/design/css/jquery.smartbanner.css" type="text/css" media="screen">
		<link href="themes/Default/design/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
		<link href="themes/Default/design/global/css/components.css" rel="stylesheet">
		<script src="themes/Default/design/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
		<script src="themes/Default/design/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
		<script src="themes/Default/design/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="themes/Default/design/js/jquery.smartbanner.js"></script>

		<link href="themes/Default/design/css/style.css" rel="stylesheet">
		<link href="themes/Default/design/css/style-responsive.css" rel="stylesheet">
		<link href="themes/Default/design/css/custom.css" rel="stylesheet">


		<script type="text/javascript">
			var $p = jQuery.noConflict();
			$p(function () { $p.smartbanner({ daysHidden: 0, daysReminder: 0, title:'MTrader' }) })

			var $a = jQuery.noConflict();
			$a(document).ready(function() {
				$a('.signin').click(function(e) {
					e.preventDefault();
					$a("#frmsignin").toggle('fast', function() {
						$a('#username').focus();
					});
					$a(this).toggleClass("btnsigninon");
					$a('#msg').empty();
				});

				$a('.signin').mouseup(function() {
					return false;
				});

			});


			var $j = jQuery.noConflict();
			$j(window).load(function() {
				$j('.flexslider').flexslider({
					animation: "slide",
					slideshow: true,
					slideshowSpeed: 10000,
					pauseOnHover: true,
					animationSpeed: 1000,
					controlNav: true
				});
			});

			function signinBoxPopUp() {
				var signinBox = document.getElementById("signin-box");
				if (signinBox != null) {
					if (signinBox.style.display == "none") {
						signinBox.style.display = "block";
					} else {
						signinBox.style.display = "none";
					}
				}
			}
			var sib = document.getElementById("header-signin-button");
			if (sib != null && sib.onclick) {
				signinBoxPopUp();
			}
		</script>
		<script src="themes/Default/highstock/highstock.js"></script>
		<script src="themes/Default/highstock/modules/exporting.js"></script>
		
		<style>
            #header{
                box-sizing: border-box;
                width: 60%; 
                margin-left: auto; margin-right:auto;
                background-color: rgb(250, 250, 250);
				border:1px solid gray;       
}
            #image{
				float:left;
				padding: 1%;  margin-top:1%; margin-bottom: 1%; width: 23%; 
                background-color: rgb(250, 250, 250);  box-sizing: border-box;
			}
            #image img{ 
                padding: 0%; margin: 0%; width: 100%; 
                background-color: rgb(250, 250, 250);  box-sizing: border-box;
}

#menu{
    margin-left: 30%; margin-right:1%;box-sizing: border-box; margin-top:1%; padding: 1%;
    }
        #menu ul{
            list-style-type: none; margin: 0px auto; 
        }
        #menu ul li{
            float:left; text-transform: uppercase; display: inline; margin: 2px; border:2px solid #4f81bd; margin: 2px;
        }
        #menu a:link, #menu a:visited{
            color:#4f81bd;
			text-decoration: none;
            padding: 6px 20px;
            display: inline-block;
            
        }
        #menu a:hover, #menu a:active{
            background-color: #4f81bd;
            color: #ffffff;
            text-shadow: none;
        }
        #menu ul li a{
            border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;
        }
		
		 body{
                font-family: Open Sans, sans-serif;
            }
			
			
				@media only screen and (min-width: 600px) and (max-width: 800px)
{
		#header{
                
                width: 90%;                 
}

}
		
		
		
		@media only screen and (min-width: 150px) and (max-width: 600px)
{
		#header{
                
                width: 90%;               
}
 #image{ 
                margin: 0%; width: 100%; 
}
 #image img{ 
                width: 30%; 
				margin-left: auto; margin-right: auto;
}

#menu{
	
	width: 100%;
    margin-left: 5%; margin-right: 5%; 
    }
	
	#menu a:link, #menu a:visited{
            padding: 5px 10px;           
        }
		
		#menu ul li a{
            border-radius: 2px; -moz-border-radius: 2px; -webkit-border-radius: 2px;
        }

}


        </style>
        <style type="text/css">
        	#ctl0_PageContent_bidLevels{
  				width:80%; 
  				margin-top:10%; 
  				padding:50px;
  				border: 1px solid #666;
			}
			#ctl0_PageContent_offerLevels{
				width:80%; 
  				margin-top:10%; 
  				padding:50px;
  				border: 1px solid #666
			}
        </style>
		
	</com:THead>

	<body>
		<com:TForm>
			
					<div id="header" class="row" >

						
								<div id="image">
								 <center><a href="http://atrade.co.za/"><img class="slides" src="themes/Default/design/img/compassLogo.png" alt="Compass Logo" /></a></center>
								</div>

						<div  id="menu">
							<center><ul >
								<li <%= empty($_GET["page"]) ? 'class="current"' : "" ; %> ><a href="http://atrade.co.za/">Home</a>
								</li>
							<li style="border:2px solid #4f81bd; margin: 2px;"<%= isset($_GET["page"]) && $_GET["page"] == 'guest.AboutUs' ? 'class="current"' : "" ; %>><a href="http://atrade.co.za/contact">Contact</a>
							<!--
</li>
<li <%= isset($_GET["page"]) &&  $_GET["page"] == 'guest.HowitWorks' ? 'class="current"' : "" ; %>><a href="index.php?page=guest.HowitWorks">How it works</a>
</li>
-->
							<com:TControl Visible="<%= $this->User->IsGuest %>">
								<li <%= isset($_GET["page"]) &&  $_GET["page"] == 'guest.OpenAccount' ? 'class="current"' : "" ; %>><a href="index.php?page=guest.OpenAccount">Open an Account</a>
								</li>
							</com:TControl>
						<!--<li <%= isset($_GET["page"]) &&  $_GET["page"] == 'guest.SecurityOverview' ? 'class="current"' : "" ; %>><a href="index.php?page=guest.SecurityOverview">Research & Quotes</a>
						</li>-->

					<com:TControl Visible="<%= $this->User->IsGuest %>">

						<li>

							<com:TCustomValidator ControlToValidate="Password" style="background:#fff; padding:5px;border:red 1px solid;" ErrorMessage="Login failed..." ValidationGroup="Login" Display="Dynamic" OnServerValidate="validateUser" />
							<com:TRequiredFieldValidator ControlToValidate="Username" ErrorMessage="The username was not provided" ValidationGroup="Login" FocusOnError="true" FocusElementID="Username" Display="None" />
							<com:TRequiredFieldValidator ControlToValidate="Password" ErrorMessage="The password was not provided" ValidationGroup="Login" Display="None" />

						</li>
						<li>
							<a href="#" class="signin btn" style="background:#A49244;color:#fff">Sign In</a>

							<div id="frmsignin" class="form-group form-horizontal">


								<com:TTextBox ID="Username" ValidationGroup="Login" cssClass="form-control" Attributes.placeholder="User ID" />
								<com:TTextBox ID="Password" ValidationGroup="Login" cssclass="form-control" Attributes.placeholder="Password" TextMode="Password" />
								<p><a  href="index.php?page=guest.ForgotPassword">Forget password</a></p>

								<p style="margin-top:3px;">
									<com:TCheckBox ID="RememberMe" style="font-size:9px" Text="Remember me"/>
									<!--									<a class="pull-right" style="pull-left" href="index.php?page=guest.ForgotPassword">Forget password</a>-->
									<com:TButton Text="Sign In" OnClick="loginButtonClicked" style="background:#A49244;color:#fff" cssClass="btn pull-right"  CausesValidation="true" ValidationGroup="Login" />
								</p>
							</div>
						</li>
					</com:TControl>
					<com:TControl Visible="<%= !$this->User->IsGuest %>">
						<com:TControl Visible="<%=  $_GET['page'] != 'client.PasswordReset'  &&  isset($this->Application->Session['__customer__']->forcePasswordReset) ? $this->Application->Session['__customer__']->forcePasswordReset == TRUE : '' %>">
                        <script type="text/javascript">
							window.location = "index.php?page=client.PasswordReset";
                        </script>
						</com:TControl>
						<li><a href="index.php?page=client.CompleteViewByAccount" >My Account</a></li>

						<li><com:TLinkButton Text="" OnClick="logoutButtonClicked" style="background-color:#4f81bd; color:#ffffff;" cssClass="btn header-button" CausesValidation="false">Sign Out
							</com:TLinkButton></li>

					</com:TControl>
					</ul></center>
			</div>

			</div>
		
<div class="main" style="border-top:2px solid #fff">
	<div class="container">

		<div class="col-md-12" >
			<p class="pull-right" style="padding:10px 0px 0 0;">WELCOME,
				<com:TLiteral Text="<%= $this->Application->Session['__customer__']->label %>"/>
				(
				<com:TLiteral Text="<%= $this->Application->Session['__customer__']->name %>"/>
				)
			</p>

		</div>
	</div>
</div>


</head>

<body>






	<com:TContentPlaceHolder ID="PageClass" />


	<com:TContentPlaceHolder ID="NavSection" />


	<com:TContentPlaceHolder ID="PageContent" />

	<com:TContentPlaceHolder ID="TestimonialContent" />

	<com:TContentPlaceHolder ID="SecurityContent" />


	<div class="footer">
		<div class="container">
			<div class="row">
				<!-- BEGIN COPYRIGHT -->
				<div class="col-md-6 col-sm-6">
					<div class="copyright">Powered by Zanibal Ebroker</div>
				</div>
				<div class="col-md-6 col-sm-6 pull-right">

					<script src="themes/Default/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
					<script src="themes/Default/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
					<ul class="social-icons">

						<li>
							<a class="facebook" data-original-title="facebook" href="#"></a>
						</li>


					</ul>
				</div>
			</div>
		</div>
	</div>





	<script>

		var $mess = jQuery.noConflict();
		MessageCurrentElemHeight = $mess(".message-block .valign-center-elem").height();
		$mess(".message-block .valign-center-elem").css("position", "absolute")
			.css ("top", "50%")
			.css ("margin-top", "-"+MessageCurrentElemHeight/2+"px")
			.css ("width", "100%")
			.css ("height", MessageCurrentElemHeight);


		var $w = jQuery.noConflict();
		$w(".mobi-toggler").on("click", function(event) {
			event.preventDefault();//the default action of the event will not be triggered
			$w(".header").toggleClass("menuOpened");
			$w(".header").find(".header-navigation").toggle(300);
		});
	</script>

	</com:TForm>
</body>

</html>
