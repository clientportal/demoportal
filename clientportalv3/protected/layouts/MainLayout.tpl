<!DOCTYPE html>
<html  >
<com:THead>
    <meta charset="utf-8">
    <title>Client Portal</title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="author" content="MTrader LLC">
	<meta name="apple-itunes-app" content="app-id=932775262">
	<meta name="google-play-app" content="app-id=com.zanibal.mtrader">
	<meta name="msApplication-ID" content="App" />
	<meta name="msapplication-TileImage" content="https://lh3.ggpht.com/NK6zSlEmpgwlExA9lqdPdV85XLE1uKf_J5iWtSduh2yjPx5ohY8uzIvjcuYkuQuyEX_Y=w300" />

    <link href="themes/Default/design/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="themes/Default/design/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="themes/Default/design/global/css/components.css" rel="stylesheet">
	<link rel="stylesheet" href="themes/Default/design/css/jquery.smartbanner.css" type="text/css" media="screen">
	<link href="themes/Default/design/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
	<link href="themes/Default/design/global/css/components.css" rel="stylesheet">
	<script src="themes/Default/design/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script src="themes/Default/design/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<script src="themes/Default/design/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="themes/Default/design/js/jquery.smartbanner.js"></script>

	<link href="themes/Default/design/css/style.css" rel="stylesheet">
	<link href="themes/Default/design/css/style-responsive.css" rel="stylesheet">
	<link href="themes/Default/design/css/custom.css" rel="stylesheet">

	
	 <style>
            #header{
                box-sizing: border-box;
                width: 50%; 
                margin-left: auto; margin-right:auto;
                background-color: rgb(250, 250, 250);
				border:1px solid gray;     
			}
			#image{
				float:left;
				padding: 2%;  margin-top:1%; margin-bottom: 1%; width: 30%; 
                background-color: rgb(250, 250, 250);  box-sizing: border-box;
			}
            #image img{ 
                padding: 0%; margin: 0%; width: 100%; 
                background-color: rgb(250, 250, 250);  box-sizing: border-box;
			}

#menu{
    margin-left: 30%; margin-right:1%; margin-top:1%; padding: 2%; margin-bottom: 1%;
       
    }
        #menu ul{
            list-style-type: none; margin: 0px auto; 
        }
        #menu ul li{
            float:left; text-transform: uppercase; display: inline; margin: 2px; border:2px solid #4f81bd; 
        }
        #menu a:link, #menu a:visited{
            color:#4f81bd;
            text-decoration: none;
            padding: 6px 20px; 
            display: inline-block;            
        }
        #menu a:hover, #menu a:active{
           background-color: #4f81bd;
            color: #ffffff;
            text-shadow: none;
        }
        #menu ul li a{
            border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;
        }
		
		
		
@media only screen and (min-width: 600px) and (max-width: 800px)
{
		#header{
                
                width: 80%;              
}

}
	
	
	
@media only screen and (min-width: 100px) and (max-width: 600px)
{
		#header{
                
                width: 80%;                 
}
 #image{ 
                margin: 0%; width: 100%; 
}
 #image img{ 
                width: 30%; 
				margin-left: auto; margin-right: auto;
}

#menu{
	
	width: 100%;
    margin-left: 5%; margin-right: 5%; padding:0px;
    }
	
	#menu a:link, #menu a:visited{
            padding: 5px 10px;            
        }
		
		#menu ul li a{
            border-radius: 2px; -moz-border-radius: 2px; -webkit-border-radius: 2px;
        }

}

        </style>
	
</com:THead>

<body>

	<com:TForm>
    <div id="header" class="row">
                    <div id="image">
                    <center><a href="http://atrade.co.za/"><img class="slides" src="themes/Default/design/img/Atrade_logo_blue.png" alt="A-Trade_Logo" /></a></center>
                    </div>
               
                    <div id="menu">
                       <!--background-color: rgb(250, 250, 250);
                 border: 1px solid black;-->
                       <center><ul >
                            <li><a href="http://atrade.co.za/">Home</a></li>
                            <li><a href="http://atrade.co.za/contact/">Contact</a></li>
                           </ul></center>
                    </div>
               
    </div>
	
	
        

						
   
   
    </head>

    <body>




			<com:TContentPlaceHolder ID="PageClass" />


            <com:TContentPlaceHolder ID="NavSection" />


            <com:TContentPlaceHolder ID="PageContent" />

            <com:TContentPlaceHolder ID="TestimonialContent" />

            <com:TContentPlaceHolder ID="SecurityContent" />

          


           


         </com:TForm>
       
    </body>

</html>
