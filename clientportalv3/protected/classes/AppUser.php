<?php
// Include TDbUserManager.php file which defines TDbUser
Prado::using('System.Security.TDbUserManager');
Prado::using('WebServiceClient');

/**
 * AppUser Class.
 * AppUser represents the user data that needs to be kept in session.
 * Default implementation keeps username and role information.
 */
class AppUser extends TDbUser
{

    private $_customer; //todo this object is not serialized and we have to use direct session access for now

    /**
     * @return return the customer object associated with the authenticated user object
     */
    public function getCustomer()
    {
        return $this->_customer;
    }

    public function setCustomer($customer)
    {
        $this->_customer = $customer;
    }

    /**
     * Creates a AppUser object based on the specified username.
     * This method is required by TDbUser. It checks the webservice
     * to see if the specified username is there. If so, a AppUser
     * object is created and initialized.
     *
     * @param string the specified username
     * @return AppUser the user object, null if username is invalid.
     */
    public function createUser($username)
    {
        $session = Prado::getApplication()->getSession();
        $session->open();
        $cust = null;

        try {
            $webservice = new WebServiceClient(
                Prado::getApplication()->Parameters['mcs-wsdl'],
                Prado::getApplication()->Parameters['ws-username'],
                Prado::getApplication()->Parameters['ws-password']);

            //Get the customer
            $cust = $webservice->getWebService()->findCustomerByPortalUserName(strtolower($username));
			
			//die(print_r($cust));

            if ($cust != null) {
               // $cust->lastLoginDttm = time();
               // $cust->lastLoginIPAddress = Prado::getApplication()->Request->UserHostAddress;
               // $webservice->getWebService()->updateCustomer($cust);

                //Store the user in the session
                $session['__customer__'] = $cust;
           }



            //Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
        } catch (SoapFault $e) {
            throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
        }


        if ($cust != null) // if found
        {
            $user = new AppUser($this->Manager);
            $user->_customer = $cust;
            $user->Name = $username; // set username
            $user->Roles = ('client'); // set role
            $user->IsGuest = false; // the user is not a guest
            //echo print_r($user); die(1);
            return $user;
        } else return null;
    }

    /**
     * Checks if the specified (username, password) is valid.
     * This method is required by TDbUser.
     * @param string username
     * @param string password
     * @return boolean whether the username and password are valid.
     */
    public function validateUser($username, $password)
    {
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        try {
            return $webservice->getWebService()->validateUser(strtolower($username), $password);
        } catch (SoapFault $e) {
            //echo $client->__getLastRequest();
            throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
        }
    }

    /**
     * @return boolean whether this user is an administrator.
     */
    public function getIsAdmin()
    {
        return $this->isInRole('admin');
    }


}

?>