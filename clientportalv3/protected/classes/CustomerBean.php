<?php

/**
 * @throws AppException
 * This class deals with capturing data and managing the lead to quote process
 */
class CustomerBean
{
    public $id;
	public $ecrmId;
    public $name;
    public $prefix;
    public $salutation;
    public $suffix;
    public $title;
    public $firstName;
    public $middleName;
    public $lastName;
    public $cellPhone;
    public $homePhone;
    public $otherPhone;
    public $officePhone;
    public $fax;
	public $mobilePhone;
    public $emailAddress1;
    public $emailAddress2;
    public $birthDate;
    public $nationality;
    public $sex;
    public $motherMaidenName;
    public $identifier;
    public $identifierType;
    public $identifierExpDate;
    public $primaryAddress1;
    public $primaryAddress2;
    public $primaryCity;
    public $primaryPostCode;
    public $primaryState;
    public $primaryCountry;
    public $secondaryAddress1;
    public $secondaryAddress2;
    public $secondaryCity;
    public $secondaryPostCode;
    public $secondaryState;
    public $secondaryCountry;
    public $portalUserName;
    public $portalPassword;
    public $customerType;
    public $partnerType;
	public $clientGroup;
	

    public $secretQuestion;
    public $secretAnswer;
    public $alertsEmail;
    public $profession;

    public $setttlementBankName;
    public $setttlementBankBranch;
    public $setttlementBankStreet;
    public $setttlementBankCity;
    public $setttlementBankPostState;
    public $setttlementBankPostCode;
    public $setttlementBankPostCountry;
    public $setttlementBankAccountName;
    public $setttlementBankAccountNumber;
    public $setttlementBankAccountSCode;
    public $setttlementBankOpenDate;


    // handles images
    public $file1LN = null;
    public $file2LN = null;
    public $file3LN = null;
    public $file4LN = null;
	public $file5LN = null;
	public $file6LN = null;
	public $file7LN = null;
	public $file8LN = null;
	public $file9LN = null;
	public $file10LN = null;
	public $file11LN = null;
	

    public $file1FT = null;
    public $file2FT = null;
    public $file3FT = null;
    public $file4FT = null;
	public $file5FT = null;
	public $file6FT = null;
	public $file7FT = null;
	public $file8FT = null;
	public $file9FT = null;
	public $file10FT = null;
	public $file11FT = null;
	

    public $file1FS = null;
    public $file2FS = null;
    public $file3FS = null;
    public $file4FS = null;
	public $file5FS = null;
	public $file6FS = null;
	public $file7FS = null;
	public $file8FS = null;
	public $file9FS = null;
	public $file10FS = null;
	public $file11FS = null;
	

    public $file1FN = null;
    public $file2FN = null;
    public $file3FN = null;
    public $file4FN = null;
	public $file5FN = null;
	public $file6FN = null;
	public $file7FN = null;
	public $file8FN = null;
	public $file9FN = null;
	public $file10FN = null;
	public $file11FN = null;
	


    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
	
	 public function setecrmId($ecrmId)
    {
        $this->ecrmId = $ecrmId;
    }

    public function getecrmId()
    {
        return $this->ecrmId;
    }
	
	public function setClientGroup($clientGroup)
    {
        $this->clientGroup = $clientGroup;
    }

    public function getClientGroup()
    {
        return $this->clientGroup;
    }

    public function setCellPhone($cellPhone)
    {
        $this->cellPhone = $cellPhone;
    }

    public function getCellPhone()
    {
        return $this->cellPhone;
    }

    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;
    }

    public function getCustomerType()
    {
        return $this->customerType;
    }

    public function setEmailAddress1($emailAddress1)
    {
        $this->emailAddress1 = $emailAddress1;
    }

    public function getEmailAddress1()
    {
        return $this->emailAddress1;
    }
	

    public function setEmailAddress2($emailAddress2)
    {
        $this->emailAddress2 = $emailAddress2;
    }

    public function getEmailAddress2()
    {
        return $this->emailAddress2;
    }
	
	public function setmobilePhone($emailAddress1)
    {
        $this->mobilePhone= $mobilePhone;
    }

    public function getmobilePhone()
    {
        return $this->mobilePhone;
    }

    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    public function getFax()
    {
        return $this->fax;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;
    }

    public function getHomePhone()
    {
        return $this->homePhone;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    public function getMiddleName()
    {
        return $this->middleName;
    }

    public function setMotherMaidenName($motherMaidenName)
    {
        $this->motherMaidenName = $motherMaidenName;
    }

    public function getMotherMaidenName()
    {
        return $this->motherMaidenName;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    public function getNationality()
    {
        return $this->nationality;
    }

    public function setOfficePhone($officePhone)
    {
        $this->officePhone = $officePhone;
    }

    public function getOfficePhone()
    {
        return $this->officePhone;
    }

    public function setOtherPhone($otherPhone)
    {
        $this->otherPhone = $otherPhone;
    }

    public function getOtherPhone()
    {
        return $this->otherPhone;
    }

    public function setPartnerType($partnerType)
    {
        $this->partnerType = $partnerType;
    }

    public function getPartnerType()
    {
        return $this->partnerType;
    }

    public function setPortalPassword($portalPassword)
    {
        $this->portalPassword = $portalPassword;
    }

    public function getPortalPassword()
    {
        return $this->portalPassword;
    }

    public function setPortalUserName($portalUserName)
    {
        $this->portalUserName = $portalUserName;
    }

    public function getPortalUserName()
    {
        return $this->portalUserName;
    }

    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function setPrimaryAddress1($primaryAddress1)
    {
        $this->primaryAddress1 = $primaryAddress1;
    }

    public function getPrimaryAddress1()
    {
        return $this->primaryAddress1;
    }

    public function setPrimaryCity($primaryCity)
    {
        $this->primaryCity = $primaryCity;
    }

    public function getPrimaryCity()
    {
        return $this->primaryCity;
    }

    public function setPrimaryCountry($primaryCountry)
    {
        $this->primaryCountry = $primaryCountry;
    }

    public function getPrimaryCountry()
    {
        return $this->primaryCountry;
    }

    public function setPrimaryPostCode($primaryPostCode)
    {
        $this->primaryPostCode = $primaryPostCode;
    }

    public function getPrimaryPostCode()
    {
        return $this->primaryPostCode;
    }

    public function setPrimaryState($primaryState)
    {
        $this->primaryState = $primaryState;
    }

    public function getPrimaryState()
    {
        return $this->primaryState;
    }

    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    public function getSalutation()
    {
        return $this->salutation;
    }

    public function setSecondaryAddress1($secondaryAddress1)
    {
        $this->secondaryAddress1 = $secondaryAddress1;
    }

    public function getSecondaryAddress1()
    {
        return $this->secondaryAddress1;
    }

    public function setSecondaryCity($secondaryCity)
    {
        $this->secondaryCity = $secondaryCity;
    }

    public function getSecondaryCity()
    {
        return $this->secondaryCity;
    }

    public function setSecondaryCountry($secondaryCountry)
    {
        $this->secondaryCountry = $secondaryCountry;
    }

    public function getSecondaryCountry()
    {
        return $this->secondaryCountry;
    }

    public function setSecondaryPostCode($secondaryPostCode)
    {
        $this->secondaryPostCode = $secondaryPostCode;
    }

    public function getSecondaryPostCode()
    {
        return $this->secondaryPostCode;
    }

    public function setSecondaryState($secondaryState)
    {
        $this->secondaryState = $secondaryState;
    }

    public function getSecondaryState()
    {
        return $this->secondaryState;
    }

    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    public function getSex()
    {
        return $this->sex;
    }

    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;
    }

    public function getSuffix()
    {
        return $this->suffix;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    public function getBirthDate()
    {
        return $this->birthDate;
    }

    public function setAlertsEmail($alertsEmail)
    {
        $this->alertsEmail = $alertsEmail;
    }

    public function getAlertsEmail()
    {
        return $this->alertsEmail;
    }

    public function setProfession($profession)
    {
        $this->profession = $profession;
    }

    public function getProfession()
    {
        return $this->profession;
    }

    public function setSecretAnswer($secretAnswer)
    {
        $this->secretAnswer = $secretAnswer;
    }

    public function getSecretAnswer()
    {
        return $this->secretAnswer;
    }

    public function setSecretQuestion($secretQuestion)
    {
        $this->secretQuestion = $secretQuestion;
    }

    public function getSecretQuestion()
    {
        return $this->secretQuestion;
    }

    public function setPrimaryAddress2($primaryAddress2)
    {
        $this->primaryAddress2 = $primaryAddress2;
    }

    public function getPrimaryAddress2()
    {
        return $this->primaryAddress2;
    }

    public function setSecondaryAddress2($secondaryAddress2)
    {
        $this->secondaryAddress2 = $secondaryAddress2;
    }

    public function getSecondaryAddress2()
    {
        return $this->secondaryAddress2;
    }

    public function setIdentifierType($identifierType)
    {
        $this->identifierType = $identifierType;
    }

    public function getIdentifierType()
    {
        return $this->identifierType;
    }

    public function setIdentifierExpDate($identifierExpDate)
    {
        $this->identifierExpDate = $identifierExpDate;
    }

    public function getIdentifierExpDate()
    {
        return $this->identifierExpDate;
    }

    public function setSetttlementBankAccountName($setttlementBankAccountName)
    {
        $this->setttlementBankAccountName = $setttlementBankAccountName;
    }

    public function getSetttlementBankAccountName()
    {
        return $this->setttlementBankAccountName;
    }

    public function setSetttlementBankAccountNumber($setttlementBankAccountNumber)
    {
        $this->setttlementBankAccountNumber = $setttlementBankAccountNumber;
    }

    public function getSetttlementBankAccountNumber()
    {
        return $this->setttlementBankAccountNumber;
    }

    public function setSetttlementBankAccountSCode($setttlementBankAccountSCode)
    {
        $this->setttlementBankAccountSCode = $setttlementBankAccountSCode;
    }

    public function getSetttlementBankAccountSCode()
    {
        return $this->setttlementBankAccountSCode;
    }

    public function setSetttlementBankBranch($setttlementBankBranch)
    {
        $this->setttlementBankBranch = $setttlementBankBranch;
    }

    public function getSetttlementBankBranch()
    {
        return $this->setttlementBankBranch;
    }

    public function setSetttlementBankCity($setttlementBankCity)
    {
        $this->setttlementBankCity = $setttlementBankCity;
    }

    public function getSetttlementBankCity()
    {
        return $this->setttlementBankCity;
    }

    public function setSetttlementBankName($setttlementBankName)
    {
        $this->setttlementBankName = $setttlementBankName;
    }

    public function getSetttlementBankName()
    {
        return $this->setttlementBankName;
    }

    public function setSetttlementBankOpenDate($setttlementBankOpenDate)
    {
        $this->setttlementBankOpenDate = $setttlementBankOpenDate;
    }

    public function getSetttlementBankOpenDate()
    {
        return $this->setttlementBankOpenDate;
    }

    public function setSetttlementBankPostCode($setttlementBankPostCode)
    {
        $this->setttlementBankPostCode = $setttlementBankPostCode;
    }

    public function getSetttlementBankPostCode()
    {
        return $this->setttlementBankPostCode;
    }

    public function setSetttlementBankPostCountry($setttlementBankPostCountry)
    {
        $this->setttlementBankPostCountry = $setttlementBankPostCountry;
    }

    public function getSetttlementBankPostCountry()
    {
        return $this->setttlementBankPostCountry;
    }

    public function setSetttlementBankPostState($setttlementBankPostState)
    {
        $this->setttlementBankPostState = $setttlementBankPostState;
    }

    public function getSetttlementBankPostState()
    {
        return $this->setttlementBankPostState;
    }

    public function setSetttlementBankStreet($setttlementBankStreet)
    {
        $this->setttlementBankStreet = $setttlementBankStreet;
    }

    public function getSetttlementBankStreet()
    {
        return $this->setttlementBankStreet;
    }


}

?>