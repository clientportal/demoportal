<?php

//Prado::using('System.Util.TEmailer');
Prado::using('System.Web.UI.ActiveControls.*');

class Contact extends TPage
{

     public function viewChanged($sender, $param)
        {
            $session = Prado::getApplication()->getSession();
                    $session->open();
                    $lq = $session['__feedback__'];
                    if ($lq == null) $lq = new FeedBackBean();

                    //Now we will bind the properties to the form model bean
                    //$lq->setTitle($this->title->Text);
                    $lq->setFirstName($this->firstName->Text);
                    $lq->setLastName($this->lastName->Text);
                    $lq->setAddressLine1($this->addressLine->Text);
                    $lq->setState($this->state->Text);
                    $lq->setCity($this->city->Text);
                    $lq->setEmail($this->email->Text);
                    $lq->setCellPhone($this->cellPhone->Text);
                    $lq->setDescription($this->description->Text);
                    $lq->setLabel($this->firstName->Text . ' ' . $this->lastName->Text);

                    //todo we will enhance the Case object to track personal details.

                    $session['__feedback__'] = $lq;

                            $doc = array(
                                "active" => true,
                                "custom" => NULL,
                                "id" => NULL,
                                "label" => 'Feedback from -> ' . $lq->getLabel(),
                                "account" => Prado::getApplication()->Parameters['case-customer-id'],
                                "salesOffice" => Prado::getApplication()->Parameters['salesoffice-id'],
                                "description" => $lq->getDescription(),
                                "origin" => "WEB",
                                "priority" => "MEDIUM",
                                "status" => "NEW",
                                "reason" => "FEEDBACK",
                                "assignedTo" => NULL,
                                "firstName" => $lq->getFirstName(),
                                "lastName" => $lq->getLastName(),
                                "addressLine1" => $lq->getAddressLine1(),
                                "state" => $lq->getState(),
                                "city" => $lq->getCity(),
                                "emailAddress" => $lq->getEmail(),
                                "country" => "NIGERIA",
                                "subject" => 'Feedback from -> ' . $lq->getLabel()
                             );

            $client = $session['__customer__'];
            if ($this->ContactPage->ActiveViewIndex == 0) {

            }
            else{
                        //Create the lead and track the ID
                        $webservice = new WebServiceClient(
                            Prado::getApplication()->Parameters['ps-wsdl'],
                            Prado::getApplication()->Parameters['ws-username'],
                            Prado::getApplication()->Parameters['ws-password']);
                        try {
                            $webservice->getWebService()->createCaseWS($doc);
                             $message = 'Your Message has been sent successfully';
                        } catch (SoapFault $e) {
                            $message = 'Unable to process request - ' . $e->faultstring;
                        }

                            $session['__profile_update_message__'] = $message;
            }
     }


}
