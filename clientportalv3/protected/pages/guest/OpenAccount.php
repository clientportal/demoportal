<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */
Prado::using('System.Web.UI.ActiveControls.*');

class OpenAccount extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        $session = Prado::getApplication()->getSession();
        $session->open();
    }

    public function viewChanged($sender, $param)
    {
        $session = Prado::getApplication()->getSession();
        $session->open();

        $ob = $session['__customer_application__'];
        if ($ob == null) {
            $ob = new CustomerBean();
            $session['__customer_application__'] = $ob;
        }

        if ($this->OpenAccount->ActiveViewIndex == 0) {
            //do nothing
        } else  if ($this->OpenAccount->ActiveViewIndex === 1 && $this->IsPostBack) {
            $indv = $this->individual->Checked;
            $private = $this->individualPrivate->Checked;
            $corp = $this->corporate->Checked;
            if( $indv == true){
                $ob->partnerType = "INDIVIDUAL";
				$ob->clientGroup = Prado::getApplication()->Parameters['customer-group-code-regular'];
            } else if( $private == true){
				$ob->partnerType = "INDIVIDUAL";
				$ob->clientGroup = Prado::getApplication()->Parameters['customer-group-code-private'];
            } else {
				$url = "index.php?page=guest.OpenAccountCorporate";
				$this->Response->redirect($url);
            }
            //$ob->partnerType = $indv == true ? "INDIVIDUAL" : "ORGANIZATION";
           // $ob->partnerType = "INDIVIDUAL";
        } else  if ($this->OpenAccount->ActiveViewIndex === 2 && $this->IsPostBack) {
            $this->bindFormValues1($ob);
        } else  if ($this->OpenAccount->ActiveViewIndex === 3 && $this->IsPostBack) {
            $this->bindFormValues2($ob);
        } else  if ($this->OpenAccount->ActiveViewIndex === 4 && $this->IsPostBack) {
            //$this->createCustomer();
			
			$this->customAttachment();
        }
    }
	
				
				private function customAttachment()
				{
					
					$session = Prado::getApplication()->getSession();
					
			//Now we should also handle the custom files if they where uploaded.
            $cb = $session['__customer_application__'];
            
			$id = $this->createCustomer();

			 //Now we will fetch the customer from the backend so that we can have the ecrm ID
			 $webservice = new WebServiceClient(
              Prado::getApplication()->Parameters['mcs-wsdl'],
              Prado::getApplication()->Parameters['ws-username'],
              Prado::getApplication()->Parameters['ws-password']);
			  $customer = $webservice->getWebService()->findCustomerById($id);
			  
			  $session['__customer_application_name__'] = $customer->name;
			 
			 //die(var_dump($customer->ecrmId));
			 //We will now upload the file if everything checks out.
            $commonDocWebService = new WebServiceClient(
                Prado::getApplication()->Parameters['cds-wsdl'],
                Prado::getApplication()->Parameters['ws-username'],
                Prado::getApplication()->Parameters['ws-password']);
				
			 if($cb->file5LN != null ) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
					$note = array(
                    "active" => true,
                    "category" => "KYC",
                    "fileMimeType" => $cb->file5FT,
                    "fileName" => $cb->file5FN,
                    "fileSize" => $cb->file5FS,
                    "label" => "BANK STATEMENT " . $customer->label,
                    "type" => "RECORD",
                    "relatedToId" => $customer->ecrmId,
                    "relatedToType" => "CUSTOMER",
                    "base64Attachment" => $cb->file5LN);		
			$commonDocWebService->getWebService()->createDocumentNoteWS($note);
			 }
			 
			 if($cb->file6LN != null ){
				 $note = array(
                    "active" => true,
                    "category" => "KYC",
                    "fileMimeType" => $cb->file6FT,
                    "fileName" => $cb->file6FN,
                    "fileSize" => $cb->file6FS,
                    "label" => "SARS DOCUMENT " . $customer->label,
                    "type" => "RECORD",
                    "relatedToId" => $customer->ecrmId,
                    "relatedToType" => "CUSTOMER",
                    "base64Attachment" => $cb->file6LN);		
			$commonDocWebService->getWebService()->createDocumentNoteWS($note);
			 }
					
					$session['__customer_application__'] = null;
				}

    private function buildSOAPDocument($cb)
    {

        $doc = array(
            "id" => NULL,
            "name" => NULL,
            "active" => true, //compulsory
            "allowDebitBalance" => false, //compulsory
            "customerGroupName" => $cb->clientGroup, //compulsory
            "businessOfficeName" => Prado::getApplication()->Parameters['business-office-code'], //compulsory
            "customerType" => "REGULAR", //compulsory
            "channel" => "WEB", //compulsory
			//"portfolioTypeName" => "WEB", //compulsory

            "setttlementBankName" => $cb->setttlementBankName,
            "setttlementBankBranch" => $cb->setttlementBankBranch,
            "setttlementBankAccountSCode" => $cb->setttlementBankAccountSCode,
            "setttlementBankAccountName" => $cb->setttlementBankAccountName,
            "setttlementBankAccountNumber" => $cb->setttlementBankAccountNumber,
            "setttlementBankOpenDate" => $cb->setttlementBankOpenDate != null
                    ? Util::convertToJavaDate($cb->setttlementBankOpenDate) : NULL,

            "picture" => $cb->file1LN, //compulsory
            "identification" => $cb->file2LN, //compulsory
            "utilityBill" => $cb->file3LN, //compulsory
            "signature" => $cb->file4LN, //compulsory
			

            "pictureFileName" => $cb->file1FN, //compulsory
            "identificationFileName" => $cb->file2FN, //compulsory
            "utilityBillFileName" => $cb->file3FN, //compulsory
            "signatureFileName" => $cb->file4FN, //compulsory

            "pictureMimeType" => $cb->file1FN, //compulsory
            "identificationMimeType" => $cb->file2FN, //compulsory
            "utilityBillMimeType" => $cb->file3FN, //compulsory
            "signatureMimeType" => $cb->file4FN, //compulsory

            "portalUserName" => $cb->portalUserName, //compulsory
            "portalPassword" => $cb->portalPassword == null ? "Welcome123" : $cb->portalPassword, //compulsory
            "secretQuestion" => $cb->secretQuestion, //compulsory
            "secretAnswer" => $cb->secretAnswer, //compulsory

            "partnerType" => $cb->partnerType, //compulsory
            "motherMaidenName" => $cb->motherMaidenName, //compulsory
            "birthDate" => $cb->birthDate != null ? Util::convertToJavaDate($cb->birthDate) : NULL,

            "homePhone" => $cb->homePhone,
            "officePhone" => $cb->officePhone,
			"cellPhone" => $cb->mobilePhone,
            "emailAddress1" => $cb->emailAddress1,

            "title" => $cb->title,
            "firstName" => $cb->firstName,
            "middleName" => $cb->middleName,
            "lastName" => $cb->lastName,

            "primaryAddress1" => $cb->primaryAddress1,
            "primaryAddress2" => $cb->primaryAddress2,
            "primaryCity" => $cb->primaryCity,
            "primaryState" => $cb->primaryState,
            "primaryPostCode" => $cb->primaryPostCode,
            "primaryCountry" => $cb->primaryCountry,
			
			//New parameters
			"dWTExemptCd" => "824",
			"dWTOverridePerc" => 0,
			"iWTExemptCd" => "455",
			"iWTOverridePerc" => 0,
			"residentStatus" => "N",
			"ficaCompliant" => "N",
                        //"taxIdNumber" => "12345",
                        //"countryOfDomicile" => "ZA",
                        //"nationality" => "ZA",
                         "taxIdNumber" => $cb->taxIdNumber,
                         "countryOfDomicile" => $cb->primaryCountry,
                          "nationality" => $cb->nationality,

     
                      "identifierType" => $cb->identifierType,
			"identifier" => $cb->identifier,
        );
        return $doc;
    }


    private function createCustomer()
    {
        $session = Prado::getApplication()->getSession();
        $ob = $session['__customer_application__'];

        //we will now create the order
        $doc = $this->buildSOAPDocument($ob);
           //die(var_dump($doc));

        //die(print_r($doc));
        //Create the quote and track the ID
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        $id = $webservice->getWebService()->createCustomer($doc);
		$session['__customer_application__']->id = $id;
		
        $session['__customer_application_id__'] = $id;
			//die(var_dump($doc));
			return $id;
    }

    public function bindFormValues1($ob)
    {
        $ob->portalUserName = $this->portalUserName->Text;
        $ob->portalPassword = $this->portalPassword->Text;
        $ob->secretQuestion = $this->secretQuestion->Text;

        $ob->secretAnswer = $this->secretAnswer->Text;
        $ob->primaryAddress1 = $this->primaryAddress1->Text;
        $ob->primaryAddress2 = $this->primaryAddress2->Text;
        $ob->primaryCity = $this->primaryCity->Text;
        $ob->primaryState = $this->primaryState->Text;
        $ob->primaryPostCode = $this->primaryPostCode->Text;
        $ob->primaryCountry = $this->primaryCountry->SelectedValue;
       //$ob->primaryCountry = $this->primaryCountry->Text;
          $ob->nationality = $this->nationality->SelectedValue;
          $ob->taxIdNumber = $this->taxId->Text; 

        $ob->title = $this->title->SelectedValue;
        $ob->firstName = $this->firstName->Text;
        $ob->middleName = $this->middleName->Text;
        $ob->lastName = $this->lastName->Text;
        $ob->motherMaidenName = $this->motherMaidenName->Text;
        $ob->birthDate = $this->birthDate->Text;

        $ob->homePhone = $this->homePhone->Text;
        $ob->officePhone = $this->officePhone->Text;
        $ob->mobilePhone = $this->mobilePhone->Text;
        $ob->emailAddress1 = $this->emailAddress1->Text;

        $ob->identifierType = $this->identifierType->SelectedValue;
        $ob->identifier = $this->identifier->Text;
        $ob->identifierExpDate = $this->identifierExpDate->Text;
    }

    public function bindFormValues2($ob)
    {
        $ob->setttlementBankName = $this->setttlementBankName->SelectedValue;
        $ob->setttlementBankAccountName = $this->setttlementBankAccountName->Text;
        $ob->setttlementBankAccountSCode = $this->setttlementBankAccountSCode->Text;
        $ob->setttlementBankAccountNumber = $this->setttlementBankAccountNumber->Text;
        $ob->setttlementBankBranch = $this->setttlementBankBranch->Text;
        $ob->setttlementBankOpenDate = $this->setttlementBankOpenDate->Text;
    }


    public function passport($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result1->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file1LN = Util::getEncodedFile($sender->LocalName);
            $cb->file1FT = $sender->FileType;
            $cb->file1FS = $sender->FileSize;
            $cb->file1FN = $sender->FileName;

        }
    }

    public function identifier($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result2->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file2LN = Util::getEncodedFile($sender->LocalName);
            $cb->file2FT = $sender->FileType;
            $cb->file2FS = $sender->FileSize;
            $cb->file2FN = $sender->FileName;

        }
    }

    public function utilitybill($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result3->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file3LN = Util::getEncodedFile($sender->LocalName);
            $cb->file3FT = $sender->FileType;
            $cb->file3FS = $sender->FileSize;
            $cb->file3FN = $sender->FileName;

        }
    }

    public function signature($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result4->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file4LN = Util::getEncodedFile($sender->LocalName);
            $cb->file4FT = $sender->FileType;
            $cb->file4FS = $sender->FileSize;
            $cb->file4FN = $sender->FileName;

        }
    }
	
	 public function bankstatement($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result5->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file5LN = Util::getEncodedFile($sender->LocalName);
            $cb->file5FT = $sender->FileType;
            $cb->file5FS = $sender->FileSize;
            $cb->file5FN = $sender->FileName;

        }
    }
	
	public function sars($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result6->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file6LN = Util::getEncodedFile($sender->LocalName);
            $cb->file6FT = $sender->FileType;
            $cb->file6FS = $sender->FileSize;
            $cb->file6FN = $sender->FileName;

        }
    }


    public function validateUsername($sender, $param)
    {
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        $param->IsValid = false; //Default to false and then switch to true if we get a SOAP Fault
        try {
            $webservice->getWebService()->findCustomerByPortalUserName(strtolower($param->Value));
        } catch (SoapFault $e) {
            $param->IsValid = true;
        }
    }
    
	public function validatePassword($sender, $param)
    {
        	//$param->IsValid = false;
		if($this->portalPassword->Text != $this->retypePortalPassword->Text ){
			$param->IsValid = false;
		}else{
			$param->IsValid = true;
		}
    }
	
    public function resetImage1($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result1->Text = "";
           $session['__customer_application__']->file1FN = null ;
           $session['__customer_application__']->file1FT = null ;
           $session['__customer_application__']->file1FS = null ;
           $session['__customer_application__']->file1LN = null ;
    }

    public function resetImage2($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result2->Text = "";
           $session['__customer_application__']->file2FN = null ;
           $session['__customer_application__']->file2FT = null ;
           $session['__customer_application__']->file2FS = null ;
           $session['__customer_application__']->file2LN = null ;
    }


    public function resetImage3($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result3->Text = "";
           $session['__customer_application__']->file3FN = null ;
           $session['__customer_application__']->file3FT = null ;
           $session['__customer_application__']->file3FS = null ;
           $session['__customer_application__']->file3LN = null ;
    }

    public function resetImage4($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result4->Text = "";
           $session['__customer_application__']->file4FN = null ;
           $session['__customer_application__']->file4FT = null ;
           $session['__customer_application__']->file4FS = null ;
           $session['__customer_application__']->file4LN = null ;
    }

	 public function resetImage5($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result5->Text = "";
           $session['__customer_application__']->file5FN = null ;
           $session['__customer_application__']->file5FT = null ;
           $session['__customer_application__']->file5FS = null ;
           $session['__customer_application__']->file5LN = null ;
    }
	
	 public function resetImage6($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result6->Text = "";
           $session['__customer_application__']->file6FN = null ;
           $session['__customer_application__']->file6FT = null ;
           $session['__customer_application__']->file6FS = null ;
           $session['__customer_application__']->file6LN = null ;
    }

}

?>
