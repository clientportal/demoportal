<?php

class ManageTradeOrder extends TPage
{


    public function onInit($param)
    {

        parent::onInit($param);
        //Set the page title
       

    }

    public function viewChanged($sender, $param)
    {
        $session = Prado::getApplication()->getSession();

        if(isset($this->Request['orderID'])) {
            $orderID = $this->Request['orderID'];
        } else{
            $url = "index.php?page=client.ListEquityOrders";
            $this->Response->redirect($url);
        }

        //Set the page title
        if($this->Request['action'] == "delete"){
            $this->Page->Title = "A-TRADE - Confirmed to cancel";
            $session['__showModifier__'] = true;
            $session['__showModifier_title__'] = "Cancel Trade Order";
        }else{
            $this->Page->Title = "A-TRADE Securities -  Order Preview";
            $session['__showModifier__'] = false;
            $session['__showModifier_title__'] = "Order Preview";
        }

        $client = $session['__customer__'];

        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        if ($this->CancelTradeOrderPage->ActiveViewIndex == 0){
            //Get a order list
            try {
                $order = $webservice->getWebService()->findTradeOrderById($orderID);
                $session['__TradeOrder_Cancel__'] =  $order;
            } catch (SoapFault $e) {
                $url = "index.php?page=client.ListEquityOrders";
                $this->Response->redirect($url);
            }

        } elseif ($this->CancelTradeOrderPage->ActiveViewIndex === 1 && $this->IsPostBack) {

            try {

                //Cancellation of Executing Trade Order
                $webservice->getWebService()->cancelTradeOrder($orderID);
                $message = 'Your request to cancel order #'.$this->Application->Session['__TradeOrder_Cancel__']->name.' has been submitted';
            } catch (SoapFault $e) {
                $message = 'Your request to cancel order #'.$this->Application->Session['__TradeOrder_Cancel__']->name.' failed: <b style="color:red">'.Util::SimplifyErrorMessage($e->getMessage()) .'</b>' ;
            }
            $session['__TradeOrder_Cancel_message__'] = $message;
        }

    }




}
