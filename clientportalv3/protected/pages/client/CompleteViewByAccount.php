<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

class CompleteViewByAccount extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - Account Summary - " . $this->Application->Session['__customer__']->label;


        if (!$this->IsPostBack) // if the page is requested the first time
        {
            $session = Prado::getApplication()->getSession();
            try {
                $webservice = new WebServiceClient(
                    Prado::getApplication()->Parameters['mcs-wsdl'],
                    Prado::getApplication()->Parameters['ws-username'],
                    Prado::getApplication()->Parameters['ws-password']);

                //Get the balances
                $defcashbal = $webservice->getWebService()->findCustomerCurrentBalanceById($session['__customer__']->id);
                $creditbal = $webservice->getWebService()->findCustomerCreditLimitById($session['__customer__']->id);
                $portfolios = $webservice->getWebService()->findCustomerPortfolios($session['__customer__']->id);
                $accounts = $webservice->getWebService()->findCustomerFiAccts($session['__customer__']->id);

                $session['__portfolios__'] = $portfolios;
                $session['__accounts__'] = $accounts;
                //die(print_r($portfolios));

                //todo calculate the net assets
                $cashbal = 0;
                $netassets = 0;
				
				if(isset($accounts->item)){
                	foreach ($accounts->item as $i) {
                    	$cashbal = $cashbal + $i->clearedBalance;
                	}
					$this->AccountRepeater->DataSource = $accounts->item;
					$this->AccountRepeater->dataBind();
				}else{
						$this->AccountRepeater->DataSource = array();
						$this->AccountRepeater->dataBind();
				}
				
				if(isset($portfolios->item)){
                	foreach ($portfolios->item as $i) {
                    	$netassets = $netassets + $i->currentValuation->amount;
                	}
					$this->PortfolioRepeater->DataSource = $portfolios->item;
					$this->PortfolioRepeater->dataBind();
				}else{
						$this->PortfolioRepeater->DataSource = array();
						$this->PortfolioRepeater->dataBind();
				}
				
                $session['__cash_balance__'] = $cashbal;
                $session['__credit_balance__'] = $creditbal;
                $session['__netassets__'] = $cashbal + $netassets;
                $session['__currency__'] = $defcashbal->currency;

                //Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
            } catch (SoapFault $e) {
                throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
            }

        }


    }
}

?>