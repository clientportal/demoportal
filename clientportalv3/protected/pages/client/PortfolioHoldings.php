<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

class PortfolioHoldings extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - Account Holdings - " . $this->Application->Session['__customer__']->label;


        if (!$this->IsPostBack) // if the page is requested the first time
        {
            $session = Prado::getApplication()->getSession();
            try {

                //Get the portfolio is not already bound
                $portfolios = $session['__portfolios__'];
                if ($portfolios == null) {
                    $webservice = new WebServiceClient(
                        Prado::getApplication()->Parameters['mcs-wsdl'],
                        Prado::getApplication()->Parameters['ws-username'],
                        Prado::getApplication()->Parameters['ws-password']);

                    $portfolios = $webservice->getWebService()->findCustomerPortfolios($session['__customer__']->id);
                    $session['__portfolios__'] = $portfolios;
                }

                if (count($portfolios->item) > 0) {
                    $this->portfolios->SelectedValue = $portfolios->item[0]->id;
                    $this->renderPortfolioData($portfolios->item[0]->id);
                }

                //Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
            } catch (SoapFault $e) {
                throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
            }

        }


    }

    private function renderPortfolioData($portfolioId)
    {
        $session = Prado::getApplication()->getSession();

        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        //Update the cash balance
        $cashbal = $webservice->getWebService()->findCustomerCurrentBalanceById($session['__customer__']->id);
        $session['__cash_balance__'] = $cashbal;

        //You have to reload the portfolio
        $portfolios = $webservice->getWebService()->findCustomerPortfolios($session['__customer__']->id);
        $session['__portfolios__'] = $portfolios;
        //Put the data in an array keyed by the portfolio ID
        $portfolio_map = array();
        foreach ($session['__portfolios__']->item as $p) {
            $portfolio_map[$p->id] = $p;
        }

        //Get the portfolio news
       // $port_news = $webservice->getWebService()->findPortfolioNewsById($portfolioId, 2);
        //$this->PortfolioNewsRepeater->DataSource = isset($port_news->item) ? $port_news->item : array();
       // $this->PortfolioNewsRepeater->dataBind();

        //Bind the portfolio drop down list
        $this->portfolios->DataSource = $session['__portfolios__']->item;
        $this->portfolios->dataBind();

//die(print_r($portfolio_map[$portfolioId]->portfolioHoldings ));
        //Bind the holdings repeater
        $phr = isset($portfolio_map[$portfolioId]->portfolioHoldings)  ? $portfolio_map[$portfolioId]->portfolioHoldings : array();
        $this->PortfolioHoldingsRepeater->DataSource = $phr;
        $this->PortfolioHoldingsRepeater->dataBind();

        //Bind the other values used to calculate the page values and display data
        $session['__current_portfolio__'] = $portfolio_map[$portfolioId];
        $session['__current_portfolio_total__'] = $portfolio_map[$portfolioId]->currentValuation->amount + $cashbal->amount;

    }


    public function updatePortfolioHoldingsPage($sender, $param)
    {
        $this->renderPortfolioData($sender->SelectedValue);
    }
}

?>