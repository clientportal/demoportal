<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

Prado::using('System.Web.UI.ActiveControls.*');

class NewFundTransaction extends TPage
{
	public function onInit($param)
	{
		parent::onInit($param);
		

		//Set the page title
		$this->Page->Title = "ClientPortal - Fund Transaction - " . $this->Application->Session['__customer__']->label;
		
		
		if (!$this->IsPostBack) // if the page is requested the first time
		{
			$session = Prado::getApplication()->getSession();
			try {

				$webservice = new WebServiceClient(
					Prado::getApplication()->Parameters['mcs-wsdl'],
					Prado::getApplication()->Parameters['ws-username'],
					Prado::getApplication()->Parameters['ws-password']);
				
				
						
				//Get the cash + overdraft
				$defcashbal = $webservice->getWebService()->findCustomerTradeBalanceById($session['__customer__']->id);
				$cashbal = $webservice->getWebService()->findCustomerTradeBalanceById($session['__customer__']->id);
				$creditbal = $webservice->getWebService()->findCustomerCreditLimitById($session['__customer__']->id);
				$session['__purchasingpower__'] = $defcashbal->amount + $creditbal->amount;
				$availbal = $webservice->getWebService()->findClearedCustomerTradeBalanceById($session['__customer__']->id);
				$session['__purchasingpower__'] = $availbal->amount + $creditbal->amount;
				$session['__cash_balance__'] = $cashbal->amount;


				//Get the portfolio list
				$portfolios = $session['__portfolios__'];
				if ($portfolios == null) {
					$portfolios = $webservice->getWebService()->findCustomerPortfolios($session['__customer__']->id);
					$session['__portfolios__'] = $portfolios;
				}			
				$this->Portfolio->DataSource =  $session['__portfolios__']->item;
				$this->Portfolio->dataBind();
				
				

				//Get the list of funds
				$funds_type = $session['__funds_type__'];
				if ($funds_type == null) {
					$funds_type = $webservice->getWebService()->findActiveSecuritiesByType("NSE", "FUND", 0, 100);
					$session['__funds_type__'] = $funds_type;
				}			
				$this->SecurityType->DataSource =  array( "" => "" ) + $session['__funds_type__']->item;
				$this->SecurityType->dataBind();

				
			}catch (SoapFault $e) {

				//die(print_r($e->faultstring));
				throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
			}


		}					

	}

	public function viewChanged($sender, $param)
	{
		$session = Prado::getApplication()->getSession();
		$client = $session['__customer__'];
		
		$ft = $session['__fundtransaction__'];
		if ($ft == null) {
			$ft = new FundTransaction();
			$session['__fundtransaction__'] = $ft;
		}
		
			if($this->FundTransaction->ActiveViewIndex == 0) {
			
				
			}else  if ($this->FundTransaction->ActiveViewIndex === 1 && $this->IsPostBack) {
			
				$this->bindFormValues($ft);
				
			}else{
				$this->saveFundTransaction($ft);
			}
			


	}
	
	public function bindFormValues($ft){
		$session = Prado::getApplication()->getSession();

		$ft->fundName = $session['__fund_properties__']->name;
		$ft->orderBase = $this->OrderBase->SelectedValue;
		$ft->transUnits = $this->Quantity->Text;
		$ft->transAmount = $this->Value->Text;
		$ft->transType = $this->TransactionType->SelectedValue;
		$ft->description = $this->Description->Text;
		$ft->currency = "NGN";
		$ft->orderDate = Util::convertToJavaDate(Util::getTodaysDate(null));
		$ft->portfolioName = $this->Portfolio->SelectedValue;
		$session['__portfolio_name__'] = $this->Portfolio->SelectedValue ;

		return $ft;
	}
	
	private function buildSOAPDocument($ft)
	{
		$session = Prado::getApplication()->getSession();		

		$doc = array(
			"id" => NULL,
			"fundName" => $ft->fundName, 
			"orderBase" => $ft->orderBase,
			"transUnits" => $ft->transUnits,
			"transAmount" => $ft->transAmount,
			"transType" => $ft->transType, 
			"orderDate" => $ft->orderDate,
			"description" => $ft->description,
			"currency" => $ft->currency,
			"portfolioName" => $ft->portfolioName,
		);		
		return $doc;
	}

	public function validateFundTransaction($sender, $param)
	{
		$session = Prado::getApplication()->getSession();
		$ft = $session['__fundtransaction__'];
		if ($ft == null) {
			$ft = new FundTransaction();
			$session['__fundtransaction__'] =  $ft;
		}

		$this->bindFormValues($ft);
		$doc = $this->buildSOAPDocument($ft);
		
		$webservice = new WebServiceClient(
			Prado::getApplication()->Parameters['mcs-wsdl'],
			Prado::getApplication()->Parameters['ws-username'],
			Prado::getApplication()->Parameters['ws-password']);

		//Will throw an EntityNotFound SOAP Fault if the there are validation errors
		//$vr = true;
		$param->IsValid = true;
		try {
			//Validate and get the total
			$orderTotal = $webservice->getWebService()->validateFundTransaction($doc);

			//Reset the validation message
			$this->ValidationMsg->Text = "";
			//$vr = false;
			$param->IsValid = true;

			//Update the order
		} catch (SoapFault $e) {
			//$vr = false;
			$param->IsValid = false;
			//process the exception
			if (Util::startsWith($e->faultstring, "INSUFFICIENT_FUNDS_FOR_BUY_ORDER")) {
				$tokens = explode("|", $e->faultstring);
				$tk1 = explode(">", $tokens[0]);
				$this->ValidationMsg->Text = "Estimated Order Total : " . $tk1[1];
			} else  if (Util::startsWith($e->faultstring, "INSUFFICIENT_SHARES_FOR_SELL_ORDER")) {
				$tokens = explode(">", $e->faultstring);
				$this->ValidationMsg->Text = "Available shares : " . $tokens[1];
			} else {
				//$this->ValidationMsg->Text = $e->faultstring;
				$token = str_replace('_', ' ', $e->faultstring);
				$this->ValidationMsg->Text = $token;
			}
		}
	}

	private function saveFundTransaction($ft)
	{

		//we will now create the fund transaction
		$doc = $this->buildSOAPDocument($ft);

		//Create the fund transaction and track the ID
		$webservice = new WebServiceClient(
			Prado::getApplication()->Parameters['mcs-wsdl'],
			Prado::getApplication()->Parameters['ws-username'],
			Prado::getApplication()->Parameters['ws-password']);

		$id = $webservice->getWebService()->createFundTransaction($doc);
		$session['__fundtransaction__'] = null;
		$session['__newfundtransactionid__'] = $id;
	}

	
	public function renderSecurityData($sender,$param)
	{
		$session = Prado::getApplication()->getSession();
		$symbol = $this->SecurityType->SelectedValue;
		
		foreach($session['__funds_type__']->item as $result)
		{
			if($result->name == $symbol){
				$session['__fund_properties__'] = $result;
				break;
			}
		}
	
	}
	

	

}

?>