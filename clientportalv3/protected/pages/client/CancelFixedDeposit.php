<?php

class CancelFixedDeposit extends TPage
{


    public function onInit($param)
    {

        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - Confirmed to cancel"  ;
    }

    public function viewChanged($sender, $param)
    {
        if(isset($this->Request['id'])) {
            $id = $this->Request['id'];
			
        } else{
            $url = "index.php?page=client.ListFixedDeposit";
            $this->Response->redirect($url);
        }


     $session = Prado::getApplication()->getSession();
            //$session->open();

     $client = $session['__customer__'];

     $webservice = new WebServiceClient(
      Prado::getApplication()->Parameters['mcs-wsdl'],
      Prado::getApplication()->Parameters['ws-username'],
      Prado::getApplication()->Parameters['ws-password']);

        if ($this->CancelTradeOrderPage->ActiveViewIndex == 0){
            //Get a order list
            try {
                $response = $webservice->getWebService()->findTermInstrumentById($id);
				
                $session['__fixedDeposit__currency'] = $response->currency ;
				$session['__fixedDeposit__defaultRate']  = $response->currentRate;
				$session['__fixedDeposit__label'] = $response->customerLabel;
				$session['__fixedDeposit__expectedInterest'] = $response->expectedInterest;
				$session['__fixedDeposit__expectedMaturity'] = $response->expectedMaturity;
				$session['__fixedDeposit__expectedNetInterest'] = $response->expectedNetInterest;
				$session['__fixedDeposit__faceValue'] = $response->faceValue;
				$session['__fixedDeposit__instrumentTypeLabel'] = $response->instrumentTypeLabel;
				$session['__fixedDeposit__name'] = $response->name;
				$session['__fixedDeposit__portfolio'] = $response->portfolioLabel ;
				$session['__fixedDeposit__rolloverRule'] = $response->rolloverRule ;
				$session['__fixedDeposit__startDate'] = $response->startDate;
				$session['__fixedDeposit__status']= $response->status;
				$session['__fixedDeposit__tenure'] = $response->tenure;
				$session['__fixedDeposit__id'] = $response->id;
				
                } catch (SoapFault $e) {
                    $url = "index.php?page=client.ListFixedDeposit";
                    $this->Response->redirect($url);
                }

        } elseif ($this->CancelTradeOrderPage->ActiveViewIndex === 1 && $this->IsPostBack) {

            try {

                //Cancellation of Executing Trade Order
                $webservice->getWebService()->deleteTermInstrument($id);
                    $message = 'The fixed deposit has been deleted successfully ';
                } catch (SoapFault $e) {
     $message = 'The fixed deposit cannot be deleted <b style="color:red">'.Util::SimplifyErrorMessage($e->getMessage()) .'</b>' ;
                }
                    $session['__FixedDeposit_Cancel_message__'] = $message;
        }

    }




}