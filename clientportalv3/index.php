<?php
date_default_timezone_set('Africa/Lagos');
//die(session_save_path());
//session_destroy();
//$frameworkPath='/Library/WebServer/Documents/prado/framework/prado.php';
//$frameworkPath='/var/www/prado/framework/prado.php';
$frameworkPath='C:/xampp/htdocs/prado/framework/prado.php';

//die(session_save_path());

// The following directory checks may be removed if performance is required
$basePath=dirname(__FILE__);
$assetsPath=$basePath.'/assets';
$runtimePath=$basePath.'/protected/runtime';

if(!is_file($frameworkPath))
	die("Unable to find prado framework path $frameworkPath.");
if(!is_writable($assetsPath))
	die("Please make sure that the directory $assetsPath is writable by Web server process.");
if(!is_writable($runtimePath))
	die("Please make sure that the directory $runtimePath is writable by Web server process.");


require_once($frameworkPath);

$application=new TApplication;
$application->run();

?>
