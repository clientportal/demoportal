<?php

include("../../../mpdf54/mpdf.php");
 
$mpdf=new mPDF('c','A4','','',15,15,16,16,9,9); 


$mpdf->mirrorMargins = 1;
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

$mpdf->debug  = true;

ob_start();
include 'CashStatement.php';
$content = ob_get_clean();
$footer = '<div align="center" style="font-size:10px; color:#000000;"><span style="font-size:10pt; color:#000000;">{PAGENO}</span><br /> ';

    $mpdf->SetHTMLFooter($footer);
	$stylesheet = file_get_contents('mpdfstyletables.css');
	$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

	$mpdf->WriteHTML($content);
         
	$mpdf->Output();
